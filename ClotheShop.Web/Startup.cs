﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClotheShop.Web.Startup))]
namespace ClotheShop.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
