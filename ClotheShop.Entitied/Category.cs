﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClotheShop.Entitied
{
   public class Category: BaseEntity
    {   
        public int ID { get; set; }

        public string Discription { get; set; }
        public List<Product> Products { get; set; }
    }
}
